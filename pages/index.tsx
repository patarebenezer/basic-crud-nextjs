import { useState } from 'react';
import { Box, Heading, Button, Table, Thead, Tbody, Tr, Th, Td, FormControl, FormLabel, Input, Textarea } from '@chakra-ui/react';
import { useQuery } from 'react-query';
import { createPost, deletePost, editPost, getAllPost } from '@/api';
import Tables from '@/app/components/Tables';

const Posts = () => {
 const fetchPosts = async () => {
  const data = await getAllPost()
  setPosts(data.slice(0, 3));
 };

 const { isLoading } = useQuery('posts', fetchPosts);
 const [selectedPost, setSelectedPost] = useState(null);
 const [posts, setPosts] = useState([]);

 const handleEdit = (post) => {
  setSelectedPost({ ...post });
 };

 const handleSave = () => {
  if (selectedPost.id) {
   editPost(selectedPost)
    .then((response) => {
     const updatedPosts = posts.map((post) => {
      if (post.id === selectedPost.id) {
       console.log(response)
       return selectedPost;
      } else {
       return post;
      }
     });
     setPosts(updatedPosts);
     setSelectedPost(null);
    })
    .catch((error) => {
     console.error(error);
    });
  } else {
   createPost(selectedPost)
    .then((response) => {
     setPosts([response.data, ...posts]);
     setSelectedPost(null);
    })
    .catch((error) => {
     console.error(error);
    });
  }
 };

 const handleDelete = async (postId) => {
  deletePost(postId)
  setPosts(posts.filter((post) => post.id !== postId));
 };

 return (
  <Box maxW="800px" mx="auto" py={8} px={4}>
   <Heading as="h1" size="xl" mb={8}>
    Posts
   </Heading>
   <Button colorScheme="blue" size="sm" mb={4} onClick={() => setSelectedPost({})}>
    New Post
   </Button>
   {isLoading ? (
    <div>Loading...</div>
   ) : (
    <Tables posts={posts} handleEdit={handleEdit} handleDelete={handleDelete}/>
   )}

   {selectedPost && (
    <Box position="fixed" top={0} bottom={0} left={0} right={0} bg="rgba(0, 0, 0, 0.4)" zIndex={100}>
     <Box
      maxW="800px"
      mx="auto"
      py={8}
      px={4}
      bg="white"
      boxShadow="md"
      borderRadius="md"
      position="relative"
      top="50%"
      transform="translateY(-50%)"
     >
      <Button
       position="absolute"
       top={2}
       right={2}
       onClick={() => setSelectedPost(null)}
       size="sm"
       variant="ghost"
      >
       X
      </Button>
      <Heading as="h2" size="lg" mb={8}>
       {selectedPost.id ? 'Edit Post' : 'New Post'}
      </Heading>

      <form>
       <FormControl mb={4} isRequired isInvalid={selectedPost && !selectedPost.title}>
        <FormLabel>Title</FormLabel>
        <Input
         name="title"
         value={(selectedPost && selectedPost.title) || ""}
         onChange={(e) =>
          setSelectedPost({
           ...selectedPost,
           title: e.target.value,
          })
         }
        />
       </FormControl>

       <FormControl mb={4} isRequired isInvalid={!selectedPost.body}>
        <FormLabel>Body</FormLabel>
        <Textarea
         name="body"
         value={selectedPost.body || ''}
         onChange={(e) => setSelectedPost({ ...selectedPost, body: e.target.value })}
        />
       </FormControl>

       <Button
        colorScheme="blue"
        size="sm"
        mr={4}
        disabled={!selectedPost.title || !selectedPost.body}
        onClick={handleSave}
       >
        {selectedPost.id ? 'Save' : 'Create'}
       </Button>
       <Button size="sm" onClick={() => setSelectedPost(null)}>
        Cancel
       </Button>
      </form>
     </Box>
    </Box>
   )}
  </Box>
 );
};

export default Posts;
