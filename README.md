# Basic CRUD Nextjs
This is a sample application built with React, Chakra UI, React Query, Axios, and React Hook Form that demonstrates how to perform CRUD operations (Create, Read, Update, Delete) on posts using the JSONPlaceholder API endpoints.


## Getting started
To install this application, follow these steps:

Clone the repository to your local machine using the command git clone https://github.com/<patarebenezer>/react-crud-posts.git.
Open the project folder in your code editor and install the required dependencies using the command npm install.

## Usage

```
To use this application, follow these steps:

Run the command npm run dev to start the development server.
Open your web browser and navigate to http://localhost:3000.
You should now see the application running.
```

## Dependencies and tools used
This application uses the following dependencies and tools:

- [ ] NextJS
- [ ] Chakra UI
- [ ] React Query
- [ ] Axios
- [ ] React Hook Form

## Contributors

Patar Ebenezer Siahaan
## License
For open source projects, say how it is licensed.

