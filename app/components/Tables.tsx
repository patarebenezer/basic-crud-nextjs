import { Button, Table, Thead, Tbody, Tr, Th, Td } from '@chakra-ui/react';
interface TableProps{
 posts: [],
 handleEdit: any,
 handleDelete: any
}

export default function Tables({ posts, handleEdit, handleDelete }: TableProps) {
 return (
  <Table variant="simple">
   <Thead>
    <Tr>
     <Th>Title</Th>
     <Th>Body</Th>
     <Th></Th>
    </Tr>
   </Thead>
   <Tbody>
    {posts.map((post) => (
     <Tr key={post.id}>
      <Td>{post.title}</Td>
      <Td>{post.body}</Td>
      <Td>
       <Button size="sm" mb={2} colorScheme="green" mr={2} onClick={() => handleEdit(post)}>
        Edit
       </Button>
       <Button
        size="sm"
        colorScheme="red"
        onClick={() => handleDelete(post.id)}
       >
        Delete
       </Button>

      </Td>
     </Tr>
    ))}
   </Tbody>
  </Table>
 )
}
