import axios from "axios";
import POST_TYPE from "./types/posts";

const API_URL = "https://jsonplaceholder.typicode.com";

export const getAllPost = async () => {
 const response = await axios.get(`${API_URL}/posts`);
 return response.data;
};

export const createPost = async (post:POST_TYPE) => {
 return await axios.post('https://jsonplaceholder.typicode.com/posts', post)
};

export const editPost = async (post:POST_TYPE) => {
 return await axios.put(`https://jsonplaceholder.typicode.com/posts/${post.id}`, post)
};

export const deletePost = async (id:POST_TYPE) => {
 return await axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`);
};
